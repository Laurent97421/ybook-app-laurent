//Libraries
import axios from "axios";

export const uploadToS3 = async (fileData: any) => {
  if (!fileData.file[0]) {
    return null;
  }

  // const fileType = encodeURIComponent(fileData.file[0].type);
  const { data } = await axios.get(`http://localhost:3001/server/upload`);
  console.log(data);

  const { uploadUrl, key } = data;

  const test = await axios.put(uploadUrl, fileData.file[0]);
  console.log(test);
  return key;
};
