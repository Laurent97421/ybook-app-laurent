//Libraries
import React from "react";

//Components
import { Center, Image } from "@chakra-ui/react";

interface HeaderProps {
  color: string;
  logoSize: string;
}

const Header: React.FC<HeaderProps> = ({ color, logoSize }) => {
  return (
    <Center h={logoSize} backgroundColor={color}>
      <Image
        boxSize={logoSize}
        objectFit="contain"
        src={`${process.env.PUBLIC_URL}/assets/images/ynov_campus.png`}
        alt="logo ynov"
      />
    </Center>
  );
};

export default Header;
