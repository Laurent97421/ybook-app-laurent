//Libraries
import React from "react";
import axios from "axios";

//Components
import { Box, Avatar, Flex, Image, Text } from "@chakra-ui/react";

//Hooks
import { useContext, useState } from "react";

//Theme
import { ThemeContext } from "../../../../contexts/theme";

//Icons
import { IoHeartOutline, IoHeartSharp, IoChatbubbleOutline } from "react-icons/io5";

interface PicturePostProps {
  filekey: string | null;
}

const PicturePost: React.FC<PicturePostProps> = ({ filekey }) => {
  const theme = useContext(ThemeContext);

  const [url, setUrl] = useState("");

  async function fetchData() {
    const { data } = await axios.get(`http://localhost:3001/server/download/${filekey}`);

    setUrl(data);
  }

  if (filekey != "") {
    fetchData();
  }
  return (
    <Box my={4}>
      <Flex direction="column">
        <Flex gap={2} align="center" pl={3} py={2}>
          <Avatar
            size="sm"
            name="Timothée Techer 9742"
            src="https://media.licdn.com/dms/image/C4E03AQHg8NmZ1yxtWA/profile-displayphoto-shrink_800_800/0/1652307186402?e=2147483647&v=beta&t=UFT6cJvunLjT1q9Uxy3cq3KWF6_J9fv8lT9ra_hL-40"
          />

          <Text fontSize="xs">Timothée Techer</Text>
        </Flex>

        <Box h="360px">
          <Image boxSize="100%" objectFit="contain" src={url ? url : ""} />
        </Box>
        <Flex pl={3} py={2}>
          <Box mr={3}>
            <IoHeartOutline size="24px" color={theme.secondary} />
          </Box>
          <IoChatbubbleOutline size="24px" color={theme.secondary} />
        </Flex>
      </Flex>
    </Box>
  );
};

export default PicturePost;
