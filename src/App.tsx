//Components
import { Route, Routes } from "react-router";

//Pages
import Home from "./containers/Home/Home";
import ChatList from "./containers/ChatList/ChatList";
import Settings from "./containers/Settings/Settings";
import NotFound from "./containers/NotFound/NotFound";
import Friends from "./containers/Friends/Friends";
import Signup from "./containers/auth/Signup/Signup";
import Login from "./containers/auth/Login/Login";
import AddPost from "./containers/AddPost/AddPost";
import ChatRoom from "./containers/ChatList/ChatRoom/ChatRoom";

//Routes
import routes from "./config/routes";

const App = () => {
  return (
    <Routes>
      <Route path={routes.HOME} element={<Home />} />
      <Route path={routes.SIGNUP} element={<Signup />} />
      <Route path={routes.LOGIN} element={<Login />} />
      <Route path={routes.FRIENDS} element={<Friends />} />
      <Route path={routes.ADD_POST} element={<AddPost />} />
      <Route path={routes.CHAT_LIST} element={<ChatList />} />
      <Route path={routes.CHAT_ROOM} element={<ChatRoom />} />
      <Route path={routes.PROFILE} element={<Settings />} />
      <Route path="*" element={<NotFound />} />
    </Routes>
  );
};

export default App;
