//Libraries
import React from "react";

//Components
import {
  Box,
  Input,
  InputGroup,
  InputLeftElement,
  VStack,
  StackDivider,
} from "@chakra-ui/react";
import ChatListItem from "../../components/ui/ChatListItem/ChatListItem";

//Hooks
import { useContext } from "react";

//Context
import { ThemeContext } from "../../contexts/theme";

//Icons
import { IoSearchOutline } from "react-icons/io5";

interface ChatListProps {}

const ChatList: React.FC<ChatListProps> = () => {
  const theme = useContext(ThemeContext);
  return (
    <Box>
      <InputGroup>
        <InputLeftElement py={6} pointerEvents="none" children={<IoSearchOutline />} />
        <Input
          type="text"
          placeholder="Rechercher"
          py={6}
          borderRadius={0}
          _focus={{
            borderColor: theme.primary,
          }}
        />
      </InputGroup>

      <VStack px={4} align="flex-start" divider={<StackDivider />} spacing={0}>
        <ChatListItem id={1} />
        <ChatListItem id={2} />
        <ChatListItem id={3} />
        <ChatListItem id={4} />
        <ChatListItem id={5} />
        <ChatListItem id={6} />
        <ChatListItem id={7} />
        <ChatListItem id={8} />
        <ChatListItem id={9} />
        <ChatListItem id={10} />
      </VStack>
    </Box>
  );
};

export default ChatList;
