//Libraries
import React, { useEffect, useState } from "react";

//Components
import {
  Avatar,
  Flex,
  Heading,
  Input,
  InputGroup,
  Box,
  InputRightElement,
  Text
} from "@chakra-ui/react";

//Hooks
import { useContext } from "react";

//Contexts
import { ThemeContext } from "../../../contexts/theme";

//Icons
import { IoSend } from "react-icons/io5";
import { io } from "socket.io-client"
import BubbleChat from "../../../components/ui/BubbleChat/BubbleChat";

const socket = io("http://localhost:3002").connect();

interface ChatRoomProps { }

const ChatRoom: React.FC<ChatRoomProps> = () => {
  const theme = useContext(ThemeContext);

  const [username, setUsername] = useState("");
  const [currentMessage, setCurrentMessage] = useState("");
  const [messageList, setMessageList] = useState([]);
  const room = "1";

  console.log("BBBBBBBBBB")
  console.log(localStorage.getItem("username"))

  useEffect(() => {
    socket.emit("join_room", room);
  },[])


  const sendMessage = async () => {
    setUsername(JSON.stringify(localStorage.getItem("username")))
    if (currentMessage !== "") {
      const messageData = {
        room: room,
        author: username,
        message: currentMessage,
        time:
          new Date(Date.now()).getHours() + ":" + new Date(Date.now()).getMinutes(),
      };

      await socket.emit("send_message", messageData);
      setMessageList((list: any): any => [...list, messageData])
      setCurrentMessage("")
    }
  };


  return (
    <Flex h="100%" direction="column">

      <Flex h="60px" align="center" gap={3} px={5} borderBottomWidth={1}>
        <Avatar
          size="sm"
          name="Timothée Techer"
          src="https://media.licdn.com/dms/image/C4E03AQHg8NmZ1yxtWA/profile-displayphoto-shrink_800_800/0/1652307186402?e=2147483647&v=beta&t=UFT6cJvunLjT1q9Uxy3cq3KWF6_J9fv8lT9ra_hL-40"
        />

        <Heading as="h6" size="sm">
          Timothée Techer
        </Heading>
      </Flex>

      <Flex flexGrow={1}>
        <BubbleChat messageList={messageList} username={username} />
      </Flex>

      <InputGroup h="60px">
        <Input
          h="100%"
          type="text"
          value={currentMessage}
          placeholder="Écrivez un message..."
          onChange={(event) => {
            setCurrentMessage(event.target.value);
          }}
          onKeyPress={(event) => {
            event.key === "Enter" && sendMessage();
          }}
          borderRadius={0}
          _focus={{
            borderColor: theme.primary,
          }}
        />
        <InputRightElement
          h="100%"
          pointerEvents="none"
          onClick={sendMessage}
          children={<IoSend color={theme.secondary} />}
        />
      </InputGroup>
    </Flex>
  );
};

export default ChatRoom;
